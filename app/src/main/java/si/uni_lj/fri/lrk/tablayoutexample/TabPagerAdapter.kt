package si.uni_lj.fri.lrk.tablayoutexample

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentActivity
import androidx.viewpager2.adapter.FragmentStateAdapter

class TabPagerAdapter(fa: FragmentActivity?, private val tabCounter: Int) : FragmentStateAdapter(fa!!) {
    override fun createFragment(position: Int): Fragment {
        //TODO("Not yet implemented")
        when (position) {
            0 -> return Tab1Fragment()
            1 -> return Tab2Fragment()
            2 -> return Tab3Fragment()
            else -> {
                return Tab1Fragment()
            }
        }
    }

    override fun getItemCount(): Int {
        //TODO("Not yet implemented")
        return tabCounter
    }
}