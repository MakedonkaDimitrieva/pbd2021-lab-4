package si.uni_lj.fri.lrk.tablayoutexample

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import androidx.viewpager2.widget.ViewPager2
import com.google.android.material.tabs.TabLayout
import com.google.android.material.tabs.TabLayoutMediator
import si.uni_lj.fri.lrk.tablayoutexample.databinding.ActivityMainBinding

class MainActivity : AppCompatActivity() {

    companion object {
        private const val TAG = "MainActivity"
        private const val NUM_OF_TABS = 3
    }

    private lateinit var binding: ActivityMainBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        //setContentView(R.layout.activity_main)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)
        //val toolbar = findViewById<Toolbar>(R.id.toolbar)
        //setSupportActionBar(toolbar)
        setSupportActionBar(binding.toolbar)
        configureTabLayout()
    }

    private fun configureTabLayout() {

        // TODO: Implement tab-viewpager-adapter connection

        //val tabLayout = findViewById<TabLayout>(R.id.tab_layout)
        val tabLayout = binding.tabLayout

        //val viewPager = findViewById<ViewPager2>(R.id.view_pager)
        val viewPager = binding.viewPager

        val tpAdapter = TabPagerAdapter(this, NUM_OF_TABS)

        viewPager.adapter = tpAdapter

        TabLayoutMediator(tabLayout, viewPager
        ) { tab, position ->
            when (position) {
                0 -> tab.setText(R.string.tab1_title)
                1 -> tab.setText(R.string.tab2_title)
                2 -> tab.setText(R.string.tab3_title)
            }
        }.attach()
    }

}